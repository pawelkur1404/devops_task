Application sample is created by command npx.

The Node.js image is used at the beginning of the Dockerfile as a builder to install the app's dependencies and create the app itself.

To serve the created app, the Dockerfile subsequently switches to the Nginx image.

The customized Nginx configuration file is copied to the container.

The container has exposed Port 80.

The CMD instruction starts Nginx and keeps the container running in the foreground.

The docker-compose file defines a single service named app.

CI/CD

To use this pipeline, you'll need to set up the environment variables in your GitLab project's CI/CD settings. 
Stage build creating docker image and push it to the Docker HUB

HELM

Helm Chart contains two main yaml files: Deployment and service.
Service is for Load Balancer.

USE :
docker build -t react_app .

then

docker-compose up